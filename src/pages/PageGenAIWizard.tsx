import GenAIPrototypeWizard from '@/components/organisms/GenAIPrototypeWizard'

const PageGenAIWizard = () => {
  return (
    <div className="flex h-full w-full">
      <GenAIPrototypeWizard />
    </div>
  )
}

export default PageGenAIWizard
